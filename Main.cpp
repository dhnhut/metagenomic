#include "evaluation.h"
#include "DNACollection.h"
#include "DNAReadFile.h"
#include "DNAKMean.h"
#include "DNAKMedoids.h"
#include <iostream>

using namespace std;

int numOfSignals = sizeof(signalNameIds)/sizeof(*signalNameIds);
int numOfFiles = sizeof(fileNames)/sizeof(*fileNames);

void RunSpecified(){
	cout<<"Filename: " << fileName << "\n";
	cout<<"Signal: " << signalNames[signalId] << "\n";
	// DNACollection collection = ReadDNA();
	// collection.ExportVectorsFile();
	// collection.ExportChartFile();

	DNACollection collection = ReadVector();

	// DNA_Kmean(&collection);

	GenDistanceMatrix(&collection);
	// DNA_KMedoids(&collection);

	// collection.ExportClusteringFile();
	// EvaluationResult();
}

void ChoiceSample() {

	//TODO: Get fileName, fileIndex, totalSpecies
	int choice = -1;

	for (int i = 0; i < numOfFiles; ++i) {
		cout << i << " - " << fileNames[i] << "\n";
	}

	do {
		cout << "Choice sample: ";
		cin >> choice;
	} while (choice < 0  || choice >= numOfFiles);

	fileName = fileNames[choice];
	fileIndex = choice;
	totalSpecies = arrTotalSpecies[choice];

	//TODO: Get signalId
	choice = -1;

	for (int i = 0; i < numOfSignals; ++i) {
		cout << i << " - " << signalNames[i] << "\n";
	}

	do {
		cout << "Choice signal: ";
		cin >> choice;
	} while (choice < 0  || choice >= numOfSignals);

	signalId = choice;

	RunSpecified();
}

void RunAll() {
	int skipCount = 645;
	int maxCount = 645; //max class 340
	int runCount = 0;
	for (int i = 0; i < numOfFiles; ++i)
	{
		fileName = fileNames[i];
		fileIndex = i;
		totalSpecies = arrTotalSpecies[fileIndex];

		for (int j = 0; j < numOfSignals; ++j)
		{
			runCount++;
			if (runCount < skipCount)
				continue;
			else if(runCount > maxCount)
				break;
			cout << "\nRUNCOUNT: " << runCount << "\n";
			signalId = j;

			RunSpecified();
		}
	}
}

int main()
{
	int choice = -1;
	do {
		cout << "Enter 0 - to run all; 1 to run specified sample\nChoice: ";
		cin >> choice;
	} while (choice != 0 && choice != 1);
	// int choice = 0;
	switch (choice) {
		case 0:
			RunAll();
			break;
		case 1:
			ChoiceSample();
			break;
	}

	cout<<"\n";
	return 0;
}