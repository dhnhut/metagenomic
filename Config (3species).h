#ifndef __CONFIG_H__
#define __CONFIG_H__ 

#include <vector>
#include <string>
#include <limits.h>
#include <stdio.h>
#include <math.h>

using namespace std;

const int KMer = 4;
const int V = 256; // vector length sqr(k,4)

const std::string Nus[] = {"A","T","G","C"};
vector<string> GenerateNuVector(int lmer);

const string diNuVector[16] = {"AA", "AT", "AG", "AC", "TA", "TT", "TG", "TC", "GA", "GT", "GG", "GC", "CA", "CT", "CG", "CC"};

const int linesPerSpecies = 50;
const int maxKmeanLoop = 200;

const string VectorFolder = "/media/Data/Result/Vectors/";
const string ResultsFolderName = "Results/";
const string SamplesFolderName = "Samples/";

const string signalNames[] = {" - 4mer - ", " - GC - ", " - FOM - ", " - SOM - "};
const int signalNameIds[] = {0, 1, 2, 3};
extern int signalId; // 0: 4-mer; 1: GC-Content; 2: FOM; 3 SOM
extern string fileName;
extern int fileIndex;
extern int totalSpecies;

const string fileNames[] = {
		"1-species02-class-10000read_exact1000",
		"1-species02-kingdom-10000read_exact1000",
		"1-species-phylum-10000read_Exact_1000",
		"2-species02-class-10000read_Exact1000",
		"2-species02-kingdom-10000read_exact1000",
		"r1",
		"r3",
		"r7",
		"01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact",
		"02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact", // 10
		"03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact",
		"04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact",
		"05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact",
		"06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact",
		"07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact", // 15
		"08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact",
		"09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact",
		"10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact",
		"11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact",
		"12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact", // 20
		"13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact",
		"14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact",
		"15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact",
		"16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact",
		"17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact", //25
		"18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact",
		"19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact",
		"20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact",
		"01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact", //30
		"03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact", //35
		"08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact", //40
		"13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact", //45
		"18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact",
		"01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact", //50
		"03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact", //55
		"08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact", //60
		"13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact", //65
		"18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact",
		"01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact", //70
		"03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact",
		"05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact", //75
		"08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact",
		"09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact", //80
		"03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact",
		"04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact",
		"05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact",
		"06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact",
		"07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact", //85
		"08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact", //90
		"05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
		"06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact",
	};

const int arrClustersSize [][15] = {
		{ 2959, 7041 }, //1-species02-class-10000read_exact1000
		{ 7073, 2927 }, //1-species02-kingdom-10000read_exact1000
		{ 6236, 3764 }, //1-species-phylum-10000read_Exact_1000
		{ 6183, 3817 }, //2-species02-class-10000read_Exact1000
		{ 6600, 3400 }, //2-species02-kingdom-10000read_exact1000
		{ 42189, 40771 }, //r1
		{ 47457,45810 }, //r3
		{ 19473, 19291, 251709 }, //r7
		{ 13946, 18103, 17951 }, // 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 15861, 12928, 21211 }, // 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 16541, 15660, 17799 }, // 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 17170, 16594, 16236 }, // 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 13671, 19046, 17283 }, // 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 16732, 16493, 16775 }, // 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 15763, 17375, 16862 }, // 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 18158, 16857, 14985 }, // 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 16844, 16973, 16183 }, // 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 17242, 16246, 16512 }, // 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 17596, 15784, 16620 }, // 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 16786, 16471, 16743 }, // 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 14153, 16514, 19333 }, // 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 16200, 17062, 16738 }, // 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 16965, 15957, 17078 }, // 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 17834, 14496, 17670 }, // 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 16855, 14545, 18600 }, // 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 14943, 16587, 18470 }, // 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 16387, 13722, 19891 }, // 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 20570, 13096, 16334 }, // 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 17399, 14272, 18329 }, // 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 14834, 16792, 18374 }, // 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 15166, 18069, 16765 }, // 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 23157, 24764, 2079 }, // 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 10507, 22551, 16942 }, // 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 14574, 16276, 19150 }, // 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 7688, 18744, 23568 }, // 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 20273, 15758, 13969 }, // 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 19790, 13010, 17200 }, // 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 3203, 19662, 27135 }, // 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 15677, 12208, 22115 }, // 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 18384, 13835, 17781 }, // 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 12950, 17255, 19795 }, // 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 12706, 16485, 20809 }, // 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 16898, 13774, 19328 }, // 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 17504, 15727, 16769 }, // 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 13082, 20055, 16863 }, // 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 18539, 13989, 17472 }, // 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 14746, 19732, 15522 }, // 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		{ 13328, 18507, 18165 }, // 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,		
		{ 22634, 18252, 9114 }, // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 20555, 14400, 15045 }, // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 17920, 13665, 18415 }, // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 19971, 15227, 14802 }, // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 6199, 22552, 21249 }, // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 25604, 4515, 19881 }, // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 16313, 9480, 24207 }, // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 11164, 13250, 25586 }, // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 20308, 11781, 17911 }, // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 16455, 18524, 15022 }, // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 16454, 18524, 15022 }, // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 24268, 11731, 14001 }, // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 16383, 16173, 17444 }, // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 17337, 19832, 12831 }, // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 5848, 19139, 17512 }, // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 11440, 19771, 18789 }, // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 21868, 10620, 17512 }, // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 24432, 13923, 11645 }, // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 8588, 22908, 18504 }, // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 15502, 14458, 20040 }, // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 13865, 12683, 23452 }, // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 17174, 13877, 18949 }, // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 26674, 11935, 11391 }, // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 21394, 14809, 13797 }, // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 18112, 29148, 2740 }, // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 15717, 13567, 20716 }, // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 14706, 17825, 17469 }, // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 13298, 16431, 20271 }, // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 10717, 11311, 27972 }, // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 4085, 24192, 21723 }, // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 14718, 17882, 17400 }, // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 13583, 19091, 17326 }, // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 12197, 22406, 15397 }, // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 13338, 19628, 17034 }, // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 13328, 19151, 17521 }, // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 6525, 30345, 13130 }, // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 25154, 18134, 6712 }, // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 19527, 16806, 13667 }, // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 17573, 14392, 18035 }, // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 27380, 8211, 14409 }, // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 20452, 24809, 4739 }, // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 15566, 21650, 12784 }, // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 25713, 10307, 13980 }, // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		{ 13954, 15745, 20301 }, // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
	};

const int arrTotalReads[] = {
		10000,	// 1-species02-class-10000read_exact1000
		10000,	// 1-species02-kingdom-10000read_exact1000
		10000,	// 1-species-phylum-10000read_Exact_1000
		10000,	// 2-species02-class-10000read_Exact1000
		10000,	// 2-species02-kingdom-10000read_exact1000
		82960,	// r1
		93267,	// r3
		290473,	// r7
		50000,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact
		50000, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		50000,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		50000,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
	};

const int arrTotalSpecies[] = {
		2,	// 1-species02-class-10000read_exact1000
		2,	// 1-species02-kingdom-10000read_exact1000
		2,	// 1-species-phylum-10000read_Exact_1000
		2,	// 2-species02-class-10000read_Exact1000
		2,	// 2-species02-kingdom-10000read_exact1000
		2,	// r1
		2,	// r3
		3,	// r7
		3,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact
		3,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact
		3, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact,
		3,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,
		3,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
		3,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact
	};

#endif