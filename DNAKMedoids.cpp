#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits.h>
#include <math.h> 
#include <float.h> 
#include <fstream>
#include <sstream>
#include "DNAKMedoids.h"

struct SwapInfo
{
	double minSwapCost;
	int mediodIndex;
	int sequenceIndex;
};

vector<int> Random_Medoids(DNACollection* collection){
	int totalReads = arrTotalReads[fileIndex];
	vector<int> pMedoids(totalSpecies);

	for (int i = 0; i < totalSpecies; ++i)
	{
		srand(time(NULL));
		pMedoids[i] = rand() % totalReads;  //number between 1 and totalReads
		//make sure no duplicate center
		for (int j = 0; j < i; ++j)
		{
			if (pMedoids[i] == pMedoids[j])
			{
				--i;
				break;
			}
		}
	}
	return pMedoids;
}

double CalculateDistance(DNASequence* first, DNASequence* second) {
	double distance = 0;

	for (int i = 0; i < V; i++)
	{
		double a = first -> _vector[i];
		double b = second -> _vector[i];
		double dis = a - b;
		distance += dis * dis;
	}

	return distance;
}

DNASequence* GetMedoidsByIndex(DNACollection* collection, int pMedoid){
	int totalReads = collection -> numOfSequence;

	// get current medoid sequences
	DNASequence* currentSequence = collection -> pFirst;
	for (int i = 0; i < totalReads; ++i)
	{
		// Check if current sequence is medoid or not
		if (i == pMedoid)
		{
			// if sequence is mediod, 
			return currentSequence;
		}

		currentSequence = currentSequence -> pNext;
	}

	return NULL;
} 

double AssignToMedoids(DNACollection* collection, vector<int> pMedoids) {
	int totalReads = collection->numOfSequence;
	DNASequence* medoids[totalSpecies];
	for (int i = 0; i < totalSpecies; ++i)
	{
		medoids[i] = GetMedoidsByIndex(collection, pMedoids[i]);
	}

	double totalCost = 0;

	DNASequence* currentSequence = collection -> pFirst;
	for (int i = 0; i < totalReads; ++i)
	{
		// Check if current sequence is medoid or not
		for (int j = 0; j < totalSpecies; ++j)
		{
			if (i == pMedoids[j])
			{
				// if sequence is mediod, 
				currentSequence -> isMedoid = true;;
				currentSequence -> clusterId = j;
				break;
			}
		}

		// if current sequence is not medoid,
		// calculate distance from it to all medoids,
		// then assign it to closest medoid cluster
		if(!currentSequence -> isMedoid){
			double minDistance = DBL_MAX;
			int minIndex = -1;
			for (int p = 0; p < totalSpecies; ++p)
			{
				double distance = CalculateDistance(medoids[p], currentSequence);
				if (distance < minDistance){
					minDistance = distance;
					minIndex = p;
				}
			}
			totalCost += minDistance;
			currentSequence -> clusterId = minIndex;
		}
		currentSequence = currentSequence -> pNext;
	}
	return totalCost;
}

SwapInfo SwapMedoids(DNACollection* collection, vector<int> pMedoids) {

	SwapInfo swapInfo;
	swapInfo.minSwapCost = DBL_MAX;
	swapInfo.mediodIndex = 0;
	swapInfo.sequenceIndex = 0;

	DNASequence* medoids[totalSpecies];
	for (int i = 0; i < totalSpecies; ++i)
	{
		medoids[i] = GetMedoidsByIndex(collection, pMedoids[i]);
	}

	int totalReads = collection->numOfSequence;
	// for each medoid
	for (int m = 0; m < pMedoids.size(); ++m)
	{
		cout<<"\npMedoids: "<< m << "\n";
		DNASequence* currentSequence = collection -> pFirst;
		for (int non_m = 0; non_m < totalReads; ++non_m)
		{
			bool is_show = (non_m%1000 == 0);
			if(is_show){
				cout<<"pNonMedoids: "<< non_m << " - ";
			}

			if(!currentSequence -> isMedoid){
				// swap current medoid
				vector<int> swapMedoids = pMedoids;
				swapMedoids[m] = non_m;
				DNASequence* swapMed = GetMedoidsByIndex(collection, non_m);
				swapMed -> isMedoid = true;
				medoids[m] -> isMedoid = false;

				// caculate total distance for this swapping
				double dis = AssignToMedoids(collection, swapMedoids);
				if(dis < swapInfo.minSwapCost){
					swapInfo.minSwapCost = dis;
					swapInfo.mediodIndex = m;
					swapInfo.sequenceIndex = non_m;
				}

				// revert swapping
				medoids[m] -> isMedoid = true;
				swapMed -> isMedoid = false;
			}
			currentSequence = currentSequence -> pNext;
		}
	}
	cout<<"\n";
	return swapInfo;
}

void DNA_KMedoids(DNACollection* collection) {
	int totalPoints = collection->numOfSequence;; //number of elements
	double minTotalCost = 0;
	// step 1: random choice medoids
	vector<int> pMedoids = Random_Medoids(collection);
	
	bool isChanged = true;
	int loop = 0;

	// step 2: assign collection to medoids
	cout<<"\npre cal cost: "<< loop << "\n";
	minTotalCost = AssignToMedoids(collection, pMedoids);
	cout<<"\npost cal cost: "<< loop << "\n";
	do
	{
		// step 3: swap each medoid to non-medoid
		cout<<"\npre cal swap: "<< loop << "\n";
		SwapInfo swapInfo = SwapMedoids(collection, pMedoids);
		cout<<"\npost cal swap: "<< loop << "\n";
		// step 4: Select lowest cost
		cout<<"\nLoop: "<< loop << " - cost: " << minTotalCost << " - swap: " << swapInfo.minSwapCost <<"\n";
		if(swapInfo.minSwapCost < minTotalCost){
			// swap to lowest change
			DNASequence* current = GetMedoidsByIndex(collection, pMedoids[swapInfo.mediodIndex]);
			DNASequence* swap = GetMedoidsByIndex(collection, swapInfo.sequenceIndex);

			current -> clusterId = swap -> clusterId;
			current -> isMedoid = false;
			swap -> isMedoid = true;

			minTotalCost = swapInfo.minSwapCost;
			pMedoids[swapInfo.mediodIndex] = swapInfo.sequenceIndex;
		}
		else {
			isChanged = false;
		}
		loop++;
	// step 5: check if medoids are changed or not to go back step 2 or exits
	} while (isChanged);
}

void GenDistanceMatrix(DNACollection* collection){
	// vector< vector<double> > matrix(totalReads, vector<double>(totalReads));
	int totalReads = arrTotalReads[fileIndex];
	string strMatrix = "";
	DNASequence* currentRowSeq = collection -> pFirst;
	for (int row = 0; row < totalReads; ++row)
	{
		cout<<"\nRow: "<<row<<"\n";
		DNASequence* currentColSeq = collection -> pFirst;
		for (int visitedRow = 0; visitedRow < row; ++visitedRow)
		{
			currentColSeq = currentRowSeq -> pNext;
			// matrix[row][visitedRow] = 0;
			strMatrix += "0,";
		}

		for (int col = 0; col < totalReads; ++col)
		{
			double distance = CalculateDistance(currentRowSeq, currentColSeq);
			// matrix[row][col] = distance;
			ostringstream ss;
			ss << distance;
			strMatrix += ss.str() + ",";
		}

		currentRowSeq = currentRowSeq -> pNext;
	}

	string fullFileName = ResultsFolderName + fileName + signalNames[signalId] + "distances.matrix";
	const char *cstr = fullFileName.c_str();
	ofstream myfile;
	myfile.open (cstr, std::ios::out);

	myfile << strMatrix;

	myfile.close();
}