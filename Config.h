#ifndef __CONFIG_H__
#define __CONFIG_H__ 

#include <vector>
#include <string>
#include <limits.h>
#include <stdio.h>
#include <math.h>

using namespace std;

const int KMer = 4;
const int V = 256; // vector length sqr(k,4)

const std::string Nus[] = {"A","T","G","C"};
vector<string> GenerateNuVector(int lmer);

const string diNuVector[16] = {"AA", "AT", "AG", "AC", "TA", "TT", "TG", "TC", "GA", "GT", "GG", "GC", "CA", "CT", "CG", "CC"};

const int linesPerSpecies = 50;
const int maxKmeanLoop = 200;

const string VectorFolder = "~/C++/Metagenomic/Results/";
const string ResultsFolderName = "Results/";
const string SamplesFolderName = "Samples/";

const string signalNames[] = {" - 4mer - ", " - GC - ", " - FOM - ", " - SOM - "};
const int signalNameIds[] = {0, 1, 2, 3};
extern int signalId; // 0: 4-mer; 1: GC-Content; 2: FOM; 3 SOM
extern string fileName;
extern int fileIndex;
extern int totalSpecies;

const string fileNames[] = {
		"1-species02-class-10000read_exact1000",
		"1-species02-kingdom-10000read_exact1000",
		"1-species-phylum-10000read_Exact_1000",
		"2-species02-class-10000read_Exact1000",
		"2-species02-kingdom-10000read_exact1000",
		"r1",
		"r3",
		"r7",
		"01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact", // 10
		"01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact", // 20
		"05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact", // 30
		"08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact", // 40
		"11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact", // 50
		"15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact", //60
		"18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact", // 70
		"01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact", // 80
		"05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact", // 90
		"08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact", // 100
		"11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact", // 110
		"15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact", // 120
		"18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact", // 130
		"01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact", // 140
		"05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact", // 150
		"08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact", // 160
		"11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact", //170
		"15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact", // 180 
		"18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact", // 190
		"01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact", // 200
		"05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact", // 210
		"08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact", // 220
		"01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact", // 230
		"05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact", // 240
		"08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact", // 250
		"03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact",
		"06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1-Exact",
		"06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2-Exact",
		"06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3-Exact", // 260
	};

const int arrClustersSize [][15] = {
		{ 2959, 7041 }, //1-species02-class-10000read_exact1000
		{ 7073, 2927 }, //1-species02-kingdom-10000read_exact1000
		{ 6236, 3764 }, //1-species-phylum-10000read_Exact_1000
		{ 6183, 3817 }, //2-species02-class-10000read_Exact1000
		{ 6600, 3400 }, //2-species02-kingdom-10000read_exact1000
		{ 42189, 40771 }, //r1
		{ 47457,45810 }, //r3
		{ 19473, 19291, 251709 }, //r7
		{ 21694, 28306 }, // 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 25047, 24953 }, // 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 21413, 28587 }, // 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 21397, 28603 }, // 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 26944, 23056 }, // 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 18803, 31197 }, // 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 25975, 24025 }, // 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 24210, 25790 }, // 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 23378, 26622 }, // 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 25482, 24518 }, // 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 25819, 24181 }, // 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 25385, 24615 }, // 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 22097, 27903 }, // 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 20507, 29493 }, // 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 26541, 23459 }, // 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 24944, 25056 }, // 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 25105, 24895 }, // 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 24956, 25044 }, // 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 23743, 26257 }, // 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 25304, 24696 }, // 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 24004, 25996 }, // 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 26162, 23838 }, // 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 27640, 22360 }, // 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 26319, 23681 }, // 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 25336, 24664 }, // 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 25156, 24844 }, // 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 25552, 24448 }, // 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 25273, 24727 }, // 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 25828, 24172 }, // 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 24816, 25184 }, // 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 24432, 25568 }, // 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 25573, 24427 }, // 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 26250, 23750 }, // 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 24230, 25770 }, // 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 24797, 25203 }, // 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 24910, 25090 }, // 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 22975, 27025 }, // 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 23108, 26892 }, // 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 21129, 28871 }, // 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 24041, 25959 }, // 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 24934, 25066 }, // 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 25263, 24737 }, // 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 24170, 25830 }, // 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 25299, 24701 }, // 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 25853, 24147 }, // 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 24728, 25272 }, // 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 28584, 21416 }, // 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 22209, 27791 }, // 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 23282, 26718 }, // 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 26781, 23219 }, // 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 21308, 28692 }, // 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 23342, 26658 }, // 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 19192, 30808 }, // 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 19941, 30059 }, // 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 22683, 27317 }, // 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 27038, 22962 }, // 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 20256, 29744 }, // 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 28004, 21996 }, // 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 30385, 19615 }, // 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 22396, 27604 }, // 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 27330, 22670 }, // 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 22217, 27783 }, // 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 24181, 25819 }, // 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 22065, 27935 }, // 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 23206, 26794 }, // 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 23854, 26146 }, // 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 23435, 26565 }, // 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 21834, 28166 }, // 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 22679, 27321 }, // 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 46018, 3982 }, // 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 45776, 4224 }, // 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 23998, 26002 }, // 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 13540, 36460 }, // 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 18996, 31004 }, // 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 30830, 19170 }, // 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 22764, 27236 }, // 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 21863, 28137 }, // 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 19563, 30437 }, // 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 14210, 35790 }, // 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 21619, 28381 }, // 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 12335, 37665 }, // 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 27817, 22183 }, // 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 27850, 22150 }, // 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 25101, 24899 }, // 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 25243, 24757 }, // 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 22838, 27162 }, // 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 15475, 34525 }, // 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 5348, 44652 }, // 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 7065, 42935 }, // 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 20203, 29797 }, // 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 23145, 26855 }, // 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 12953, 37047 }, // 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 11979, 38021 }, // 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 25437, 24563 }, // 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 28393, 21607 }, // 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 22419, 27581 }, // 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 22589, 27411 }, // 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 14367, 35633 }, // 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 11623, 38377 }, // 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 18430, 31570 }, // 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 21750, 28250 }, // 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 21332, 28668 }, // 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 23307, 26693 }, // 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 27360, 22640 }, // 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 20715, 29285 }, // 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 25012, 24988 }, // 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 26662, 23338 }, // 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 23487, 26513 }, // 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 27141, 22859 }, // 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 22119, 27881 }, // 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 19837, 30163 }, // 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 28543, 21457 }, // 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 21668, 28332 }, // 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 25516, 24484 }, // 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 18653, 31347 }, // 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		{ 22699, 27301 }, // 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		{ 22685, 27315 }, // 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		{ 21242, 28758 }, // 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,		 - 1
		{ 20772, 29228 }, // 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,		 - 2
		{ 25271, 24729 }, // 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact,		 - 3
		{ 0, 0 }, // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 20923, 29077 }, // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		{ 0, 0 }, // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		{ 0, 0 }, // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		{ 0, 0 }, // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
	};

const int arrTotalReads[] = {
		10000,	// 1-species02-class-10000read_exact1000
		10000,	// 1-species02-kingdom-10000read_exact1000
		10000,	// 1-species-phylum-10000read_Exact_1000
		10000,	// 2-species02-class-10000read_Exact1000
		10000,	// 2-species02-kingdom-10000read_exact1000
		82960,	// r1
		93267,	// r3
		290473,	// r7
		50000,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		50000, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		50000, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		50000,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		50000,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		50000,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		50000,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
	};

const int arrTotalSpecies[] = {
		2,	// 1-species02-class-10000read_exact1000
		2,	// 1-species02-kingdom-10000read_exact1000
		2,	// 1-species-phylum-10000read_Exact_1000
		2,	// 2-species02-class-10000read_Exact1000
		2,	// 2-species02-kingdom-10000read_exact1000
		2,	// r1
		2,	// r3
		3,	// r7
		2,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 01 - Genus Bacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 02 - Genus Bartonella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 03 - Genus Bifidobacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 04 - Genus Blattabacterium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 05 - Genus Bordetella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 06 - Genus Borrelia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 07 - Genus Brachyspira - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 08 - Genus Bradyrhizobium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 09 - Genus Francisella - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 10 - Genus Caldicellulosiruptor - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 11 - Genus Campylobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 12 - Genus Geobacillus - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 13 - Genus Caulobacter - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 14 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,	// 15 - Genus Chlamydophia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 16 - Genus Clostridium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 17 - Genus Corynebacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 18 - Genus Deinococcus - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 19 - Genus Desulfitobacterium - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 20 - Genus Desulfotomaculum - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2,	// 01 - Family Campylobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2,	// 02 - Family Halomonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2,	// 03 - Family Acetobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2,	// 04 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2,	// 05 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2,	// 06 - Family Nostocaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 07 - Family Clostridiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 08 - Family Deferribacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 09 - Family Rhodobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 10 - Family Enterobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 11 - Family Burkholderiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 12 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 13 - Family Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 14 - Family Bacillaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 15 - Family Geodermatophilaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 16 - Family Alteromonadaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 17 - Family Acidobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 18 - Family Pasteurellaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 19 - Family Halobacteriaceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 1
		2, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 2
		2, 	// 20 - Family Helicobacteraceae - 3 species - 50000 reads - 1000(100) bps - Exact, - 3
		2,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 01 - Order Chroococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 02 - Order Cytophagales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 03 - Order Clostridiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 04 - Order Burkholderiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 05 - Order Actinomycetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 06 - Order Flavobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 07 - Order Rhizobiaceae - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 08 - Order Oceanospirillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 09 - Order Bacteroidales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 10 - Order Chromatiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 11 - Order Alteromonadales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 12 - Order Bacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 13 - Order Rickettsiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 14 - Order Rhizobiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 15 - Order Spirochaetales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 16 - Order Lactobacillales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 17 - Order Thermoanaerobacterales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 18 - Order Sphingobacteriales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 19 - Order Chlamydiales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 20 - Order Myxococcales - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 01 - Class Alphaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 02 - Class Mollicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 03 - Class Betaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 04 - Class Thermoprotei - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 05 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 06 - Class Actinobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 07 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 08 - Class Clostridia - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 09 - Class Deltaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 10 - Class Gammaproteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 01 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 02 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 03 - Phylum Firmicutes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 04 - Phylum Bacteroidetes - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 05 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 06 - Phylum Chloroflexi - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 07 - Phylum Cyanobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 08 - Phylum Proteobacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 01 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 02 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 03 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 04 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 05 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
		2,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 1
		2,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 2
		2,  // 06 - Kingdom Bacteria - 3 species - 50000 reads - 1000(100) bps - Exact - 3
	};

#endif